select country ,count(CustomerID)
from Customers
group by country;

select ShipperName , count(Orders.OrderID) as numberOfOrders 
from Shippers join Orders on Shippers.ShipperID = Orders.ShipperID 
group by ShipperName 
order by numberOfOrders desc ;
